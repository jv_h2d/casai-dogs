import React, { useState } from 'react';
import './App.css';
import { AppRouter } from './routers/AppRouter';
import { DogContext } from "./Components/DogContext"


const App = () => {
  const [list, setDog] = useState([]);
  return (
    // <AppRouter />
    <div className="page-body">
      <DogContext.Provider value={{
        list,
        setDog
        }}>

        <AppRouter />

      </DogContext.Provider>
    </div>
  );
}

export default App;