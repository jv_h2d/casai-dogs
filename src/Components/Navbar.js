import React, { Component, useContext } from 'react'
import { Navbar, Container, Nav,  } from 'react-bootstrap'
import {LinkContainer} from 'react-router-bootstrap'
import { DogContext } from './DogContext'


const Navibar = () => {
        const { list } = useContext(DogContext);

        console.log( list );
        return (
            <div className="Navbar">
            <Navbar bg="light" expand="lg" fixed="top">
            <Container>
            <LinkContainer to="/">
                <Navbar.Brand>Dogs</Navbar.Brand>
            </LinkContainer>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse className="custom" id="basic-navbar-nav">
                    <Nav className="me-auto">
                    <LinkContainer to="/"><Nav.Link >Dogs</Nav.Link></LinkContainer>
                    <LinkContainer to="/adopted"><Nav.Link>Adopted</Nav.Link></LinkContainer>
                    </Nav>
                </Navbar.Collapse>
                <Navbar.Collapse className="justify-content-end">
                <LinkContainer to="/cart"><Nav.Link className="cart-link">Cart <p className="cart-quantity">{list.length}</p> </Nav.Link></LinkContainer>
                </Navbar.Collapse>
            </Container>
          </Navbar>
            </div>
        )
}

export default Navibar