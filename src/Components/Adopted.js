import React, { useContext } from 'react'
import { Card} from 'react-bootstrap'
import { DogContext } from './DogContext'

export const Adopted = () => {
    const { list } = useContext(DogContext);

    console.log( list );
    return (
        <div>
            <h1>Adopted screen</h1>
            <div className="row adopted-container row-cols-1 row-cols-lg-3 row-cols-md-2 row-cols-sm-1">
                {
                    list.map((dog, index)=>{
                        return (
                        <div key={dog.id} className="col mb-4 px-8 ">
                            <Card>
                            <Card.Img variant="top" src={dog.url} />
                            <Card.Body>
                                <Card.Title>{dog.title}</Card.Title>
                            </Card.Body>
                            </Card>
                        </div>
                        )
                    })
                }
            </div>
        </div>
    )
}
