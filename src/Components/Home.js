import React, {useContext} from 'react'
import { Card, Button} from 'react-bootstrap'
import dog from "../resources/puppies.json"
import { DogContext } from './DogContext'


export const Home = () => {
    const { list, setDog } = useContext( DogContext );

    return (
        <div className="App">
            <div className="row row-cols-1 row-cols-lg-3 row-cols-md-2 row-cols-sm-1">
                {
                dog.data.map((dog, index)=>{
                    if(list.length > 0){
                        for(let i = 0; i < list.length; i++){
                            if(dog.id === list[i].id){
                                return null
                            }
                        }
                    }
                    return (
                        
                    <div key={dog.id} className="col mb-4 px-8 ">
                        <Card>
                        <Card.Img variant="top" src={dog.url} />
                        <Card.Body>
                            <Card.Title>Adopt me! 🥺</Card.Title>
                            <Card.Text>{dog.title}</Card.Text>
                            <Button 
                            variant="primary"
                            onClick={ ()=> setDog((current)=>[...current,dog])}
                            >
                            Adoptar</Button>
                        </Card.Body>
                        </Card>
                    </div>
                    )
                })
                }
            </div>
        </div>
    )
}
