import React, {useContext} from 'react'
import { Card, Button} from 'react-bootstrap'
import { DogContext } from './DogContext'

export const Cart = () => {
    const { list, setDog } = useContext(DogContext);

    console.log( list );
    const removeDog = (index)=> {
        setDog(list.filter((item, i) => i !== index));
    }
    return (
        <div>
            <h1>Cart screen</h1>
            <div className="row cart-container">
                {
                list.map((list, index)=>{
                    return (
                    <div key={list.id} className="col mb-4 px-8 ">
                        <Card>
                        <Card.Img variant="top" src={list.url} />
                        <Card.Body>
                            
                            <Card.Text>{list.title}</Card.Text>
                            <Button 
                            variant="danger"
                            onClick={ ()=> removeDog(index) }
                            >
                            Delete</Button>
                        </Card.Body>
                        </Card>
                    </div>
                    )
                })
                }
            </div>
        </div>
    )
}
