import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import { Cart } from "../Components/Cart";
import { Adopted } from "../Components/Adopted";
import { Home } from "../Components/Home"
import Navibar from "../Components/Navbar";


export const AppRouter = () => {
    return (
        <Router>
          <div>
            <Navibar />
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/cart" component={Cart} />
                <Route exact path="/adopted" component={Adopted} />
                <Redirect to="/" />
            </Switch>
          </div>
        </Router>
      );
}
